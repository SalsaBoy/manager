package salsaboy.manager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.Font;
import java.awt.GridLayout;
import java.io.IOException;

public class InfoPanel extends JPanel
{
	private static final Logger logger = LogManager.getLogger(InfoPanel.class);

	private Game toShow;

	private JLabel header = new JLabel();
	private JLabel filepath = new JLabel();

	private JButton start = new JButton("Play");

	public InfoPanel()
	{
		setLayout(new GridLayout(0, 1));

		add(header);
		add(filepath);

		header.setFont(new Font("Arial", Font.BOLD, 24));
		filepath.setFont(new Font("Arial", Font.ITALIC, 12));
	}

	public void showGame(Game toShow)
	{
		this.toShow = toShow;

		header.setText(this.toShow.getGameName());
		filepath.setText(this.toShow.getLaunchPath());
		start.addActionListener(e ->
		{
			try
			{
				String command;
				if (this.toShow.isInConsole())
				{
					command = String.format("/usr/bin/osascript -e \"tell application \\\"Terminal\\\" to do script \\\"%s\\\"\"", this.toShow.getLaunchPath().replaceAll(" ", "\\ ").replaceAll("\"", "\\\""));
				} else
				{
					//Now it does start the program. Did you really think you could pass the app executable as the app itself?
					command = String.format("open -a %s", this.toShow.getLaunchPath().replace(" ", "\\ "));
				}

				String[] commandar = command.split("(?<!\\\\) ");
				for (int i = 0; i < commandar.length; i++)
				{
					commandar[i] = commandar[i].replaceAll("\\\\ ", " ");
				}

				Process process = new ProcessBuilder()
						.command(commandar)
						.inheritIO() // TODO later: change to dedicated log file?
						.start();
			} catch (IOException iox)
			{
				logger.error("Unable to launch game", iox);
			}
		});
		add(start);
	}
}
