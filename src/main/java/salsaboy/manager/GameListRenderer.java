package salsaboy.manager;

import javax.swing.*;
import java.awt.*;

public class GameListRenderer implements ListCellRenderer<Game>
{

	@Override
	public Component getListCellRendererComponent(JList<? extends Game> list, Game value, int index, boolean isSelected, boolean cellHasFocus)
	{
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout());
		panel.setBackground(Color.decode(Settings.ui.getProperty(isSelected ? "list.games.selected.background" : "list.games.unselected.background")));

//		panel.add(new JLabel(new ImageIcon()));

		JLabel name = new JLabel(value.getGameName());
		name.setForeground(Color.decode(Settings.ui.getProperty(isSelected ? "list.games.selected.foreground" : "list.games.unselected.foreground")));

		panel.add(name);

		return panel; //TODO
	}
}
