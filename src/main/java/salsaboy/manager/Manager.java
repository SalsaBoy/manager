package salsaboy.manager;

import net.miginfocom.swing.MigLayout;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import salsaboy.manager.popups.GameDialogue;

import javax.swing.*;
import java.awt.BorderLayout;

public class Manager
{
	private static final Logger logger = LogManager.getLogger(Manager.class);

	private static JFrame frame = new JFrame(Settings.strings.getProperty("window.title"));

	private static JPanel gamelistPanel = new JPanel();
	private static JButton addGame = new JButton(Settings.strings.getProperty("button.addGame"));
	private static DefaultListModel<Game> list = new DefaultListModel<>();
	private static JList<Game> gamelist = new JList<>(list);
	
	private static InfoPanel infoPanel = new InfoPanel();

	public static void main(String[] args)
	{
		gamelist.setCellRenderer(new GameListRenderer());

		frame.setLayout(new MigLayout());
		
		gamelistPanel.setLayout(new BorderLayout());
		gamelist.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		gamelistPanel.add(gamelist);
		addGame.addActionListener(e ->
		{
			GameDialogue choice = new GameDialogue(Settings.strings.getProperty("newGame.title"), Settings.strings.getProperty("newGame.name"), Settings.strings.getProperty("newGame.filePath"));
			choice.addSubmitListener(popup ->
			{
				list.addElement(new Game(popup.getFieldA(), popup.getFieldB(), popup.isUsingConsole()));
				infoPanel.showGame(list.lastElement());
			});
		});
		gamelist.addListSelectionListener(e -> infoPanel.showGame(list.elementAt(gamelist.getSelectedIndex())));
		gamelistPanel.add(addGame, BorderLayout.SOUTH);
		frame.add(gamelistPanel, "west");
		
		frame.add(infoPanel);

		frame.setSize(640, 640);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}
