package salsaboy.manager.popups;

import net.miginfocom.swing.MigLayout;
import salsaboy.manager.Settings;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class GameDialogue extends JFrame
{
	private static final Dimension TEXTBOX_SIZE = new Dimension(100, 20);

	private JTextField fieldA = new JTextField(), fieldB = new JTextField();
	private JCheckBox useConsole = new JCheckBox("Use Console");
	private boolean submitted;
	private JButton submit = new JButton(Settings.strings.getProperty("gameDialogue.submit"));

	private List<Consumer<GameDialogue>> onSubmitted = new ArrayList<>();

	public GameDialogue(String query, String field1, String field2)
	{
		super(Settings.strings.getProperty("window.title"));

		setLayout(new MigLayout("wrap 3"));

		submit.addActionListener(e ->
		{
			submitted = true;
			for (Consumer<GameDialogue> predicate : onSubmitted)
			{
				predicate.accept(this);
			}
			dispose();
		});

		add(new JLabel(query), "span 3");

		add(new JLabel(field1));
		fieldA.setPreferredSize(TEXTBOX_SIZE);
		add(fieldA, "span 2");

		add(new JLabel(field2));
		fieldB.setPreferredSize(TEXTBOX_SIZE);
		add(fieldB, "span 2");

		add(useConsole, "span 3");

		add(submit, "span 3");

		pack();
		if (getWidth() < 200) setSize(200, getHeight());
		setResizable(false);
		setVisible(true);
	}

	public void addSubmitListener(Consumer<GameDialogue> onSubmit)
	{
		onSubmitted.add(onSubmit);
	}

	public String getFieldA()
	{
		return fieldA.getText();
	}

	public String getFieldB()
	{
		return fieldB.getText();
	}

	public boolean isUsingConsole()
	{
		return useConsole.isSelected();
	}

	public boolean wasSubmitted()
	{
		return submitted;
	}
}
