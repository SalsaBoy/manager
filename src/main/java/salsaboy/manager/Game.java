package salsaboy.manager;

public class Game
{
	private String gameName, launchPath;
	private int secondsInGame;
	private boolean inConsole;

	public Game(String gameName, String launchPath, boolean useConsole)
	{
		this.gameName = gameName;
		this.launchPath = launchPath;
		this.inConsole = useConsole;
	}

	public String getGameName()
	{
		return gameName;
	}

	public void setGameName(String gameName)
	{
		this.gameName = gameName;
	}

	public String getLaunchPath()
	{
		return launchPath;
	}

	public void setLaunchPath(String launchPath)
	{
		this.launchPath = launchPath;
	}

	public int getSecondsInGame()
	{
		return secondsInGame;
	}
	
	public void setSecondsInGame(int secondsInGame)
	{
		this.secondsInGame = secondsInGame;
	}
	
	public boolean isInConsole()
	{
		return inConsole;
	}
	
	public void setInConsole(boolean inConsole)
	{
		this.inConsole = inConsole;
	}
}
