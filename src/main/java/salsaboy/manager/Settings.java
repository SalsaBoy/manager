package salsaboy.manager;

import javax.swing.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.logging.log4j.*;
import org.jetbrains.annotations.NonNls;

@NonNls
public final class Settings
{
	private static final Logger logger = LogManager.getLogger(Settings.class);

	public static final String SETTINGS_PATH;

	static
	{
		String os = System.getProperty("os.name").toLowerCase();
		if (os.contains("mac"))
		{
			SETTINGS_PATH = "~/Library/Application Support/Manager/settings.properties"; //Default mac settings path
		} else
		{
			SETTINGS_PATH = "settings.properties"; // Relative to jar file location
		}
		//TODO: Add more OS'es
	}

	public static final Properties general = new Properties();
	public static final Properties ui = new Properties();

	public static final Properties strings = new Properties();

	static
	{
		logger.info("Loading general settings");
		logger.debug("Loading default general settings");
		try
		{
			general.load(Manager.class.getResourceAsStream("/defaultGeneralSettings.properties"));
		} catch (IOException iox)
		{
			logger.error("Unable to load default settings", iox);
		}

		logger.debug("Loading local general settings");
		try (FileInputStream fis = new FileInputStream(SETTINGS_PATH))
		{
			general.load(fis);
		} catch (FileNotFoundException fnfx)
		{
			logger.debug("Local general settings not found. Creating.");
			try
			{
				File settingsFile = new File(SETTINGS_PATH);
				settingsFile.getParentFile().mkdirs();
				settingsFile.createNewFile();
			} catch (IOException iox)
			{
				logger.error("Could not create local general settings", iox);
			}
		} catch (IOException iox)
		{
			logger.error("Could not load local general settings", iox);
		}

		logger.info("Loading language settings");
		String language = general.getProperty("language");
		try
		{
			switch (language)
			{
				case "en_us":
					strings.load(Manager.class.getResourceAsStream("/languages/en_us.properties"));
					break;
			}
		} catch (FileNotFoundException fnfx)
		{
			logger.warn("Settings at {} could not be found", general.getProperty("language"), fnfx);
			JOptionPane.showMessageDialog(
					null,
					String.format("The UI settings file %s could not be found", general.getProperty("language")),
					"Error finding settings",
					JOptionPane.WARNING_MESSAGE //TODO: Custom icon?
			);
		} catch (IOException iox)
		{
			logger.error("Could not load language settings", iox);
		}

		logger.info("Loading UI settings");
		String uiSettingsLoc = general.getProperty("ui.settings");
		try
		{
			ui.load(Manager.class.getResourceAsStream("/defaultUiSettings.properties"));
		} catch (IOException iox)
		{
			logger.error("Unable to load default UI settings", iox);
		}

		if (uiSettingsLoc != null)
		{
			for (String filePath : uiSettingsLoc.split(","))
			{
				try (FileInputStream fis = new FileInputStream(filePath))
				{
					ui.load(fis);
				} catch (FileNotFoundException fnfx)
				{
					logger.warn("Settings at {} could not be found", filePath, fnfx);
					JOptionPane.showMessageDialog(
							null,
							String.format("The UI settings file %s could not be found", filePath),
							"Error finding settings",
							JOptionPane.WARNING_MESSAGE //TODO: Custom icon?
					);
				} catch (IOException iox)
				{
					logger.error("Could not load settings at {}", filePath, iox);
				}
			}
		}

		logger.info("Finished loading settings");
	}
}
